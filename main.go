package main

import (
	"bufio"   // Используется для буферизованного ввода (чтения) данных
	"fmt"     // Используется для форматированного ввода-вывода данных (Ошибки)
	"os"      // Для чтения ввода от пользователя
	"strings" // Содержит функции разделение (split) и обрезка пробелов (trim)
)

func main() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Введите выражение в формате '1 + 2': ")
	input, _ := reader.ReadString('\n') // Читает строку, введенную пользователем до символа новой строки ('\n')
	input = strings.TrimSpace(input)    // Удаляет все начальные и конечные пробелы и символы новой строки из строки ввода

	expression := strings.Split(input, " ")
	if len(expression) != 3 { // В этой части кода происходит разделение входной строки на три составляющие (число, оператор, число) с помощью функции strings.Split. Затем происходит проверка с помощью условия if len(expression) != 3.
		fmt.Println("Ошибка: неверный формат ввода")
		return
	}

	num1, err := convertToNumber(expression[0])
	if err != nil { // Эта проверка обеспечивает обработку ПЕРВОГО пользовательского значения (число или римская цифра) не соответствует ожидаемому формату
		fmt.Println("Ошибка:", err)
		return
	}

	num2, err := convertToNumber(expression[2])
	if err != nil { // Эта проверка обеспечивает обработку ВТОРОГО пользовательского значения (число или римская цифра) не соответствует ожидаемому формату
		fmt.Println("Ошибка:", err)
		return
	}

	result, err := calculate(num1, num2, expression[1])
	if err != nil { // Эта проверка обеспечивает обработку пользовательского значения (операция) не соответствует ожидаемому формату
		fmt.Println("Ошибка:", err)
		return
	}

	fmt.Println("Результат:", result)
}

func convertToNumber(str string) (int, error) {
	numbers := map[string]int{
		"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "10": 10, // Функция принимает строку как аргумент и возвращает целое число и ошибку (если есть). Она использует ассоциативный массив для соответствия строкового представления чисел и римских цифр их целочисленному значению
		"I": 1, "II": 2, "III": 3, "IV": 4, "V": 5, "VI": 6, "VII": 7, "VIII": 8, "IX": 9, "X": 10,
	}

	if val, ok := numbers[str]; ok { // Условие пытается найти значение числа в карте numbers. Если значение присутствует в массиве, функция возвращает его как целое число
		return val, nil
	}
	return 0, fmt.Errorf("неверное число: %s", str)
}

func calculate(num1, num2 int, operator string) (int, error) { // Функция принимает два числа и оператор в качестве аргументов, а затем выполняет соответствующую математическую операцию
	switch operator {
	case "+":
		return num1 + num2, nil
	case "-":
		return num1 - num2, nil
	case "*":
		return num1 * num2, nil
	case "/":
		if num2 == 0 {
			return 0, fmt.Errorf("деление на ноль")
		}
		return num1 / num2, nil
	default:
		return 0, fmt.Errorf("неверная операция: %s", operator)
	}
}
